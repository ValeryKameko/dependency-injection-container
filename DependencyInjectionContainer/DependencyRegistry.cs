using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using DependencyInjectionContainer.DependencyKey;

namespace DependencyInjectionContainer
{
    internal class DependencyRegistry : IDependencyRegistry
    {
        private readonly ConcurrentDictionary<IDependencyKey, ConcurrentStack<Dependency>> _dependenciesByKey =
            new ConcurrentDictionary<IDependencyKey, ConcurrentStack<Dependency>>();

        public void Register(IDependencyKey key, Dependency dependency)
        {
            ConcurrentStack<Dependency> UpdateDependencies(IDependencyKey dependenciesKey,
                ConcurrentStack<Dependency> dependencies)
            {
                dependencies.Push(dependency);
                return dependencies;
            }

            var dependenciesStack = new ConcurrentStack<Dependency>(new[] {dependency});
            _dependenciesByKey.AddOrUpdate(key,
                _ => dependenciesStack,
                UpdateDependencies);
        }

        public bool TryGet(IDependencyKey key, out Dependency dependency)
        {
            if (_dependenciesByKey.TryGetValue(key, out ConcurrentStack<Dependency> dependencies))
                return dependencies.TryPeek(out dependency);
            dependency = null;
            return false;
        }

        public bool TryGetAll(IDependencyKey key, out IEnumerable<Dependency> dependencies)
        {
            bool isFound = _dependenciesByKey.TryGetValue(key, out ConcurrentStack<Dependency> dependenciesStack);
            dependencies = dependenciesStack?.Reverse().ToList();
            return isFound;
        }
    }
}