using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace DependencyInjectionContainer.Utility
{
    public delegate IEnumerable CollectionCreator(Type itemType,
        IEnumerable<object> elements);

    public static class CollectionsHelper
    {
        private static readonly IReadOnlyDictionary<Type, CollectionCreator> CollectionTypes =
            new Dictionary<Type, CollectionCreator>
            {
                {typeof(List<>), CreateListLikeCollectionCreator(typeof(List<>))}
            };

        public static Type GetSupportedCollection(Type collectionDefinition)
        {
            return CollectionTypes.Keys
                .FirstOrDefault(collectionDefinition.IsAssignableFromGeneric);
        }

        public static CollectionCreator GetCollectionCreator(Type collectionDefinition)
        {
            if (CollectionTypes.ContainsKey(collectionDefinition))
                return CollectionTypes[collectionDefinition];
            return CollectionTypes
                .Where(keyValue => collectionDefinition.IsAssignableFromGeneric(keyValue.Key))
                .Select(keyValue => keyValue.Value)
                .FirstOrDefault();
        }

        private static CollectionCreator CreateListLikeCollectionCreator(
            Type collectionDefinition)
        {
            IEnumerable ListLikeCollectionCreator(Type itemType, IEnumerable<object> elements)
            {
                Type constructedCollectionType = collectionDefinition.MakeGenericType(itemType);
                var list = Activator.CreateInstance(constructedCollectionType) as IList;
                foreach (object item in elements) list.Add(item);
                return list;
            }

            return ListLikeCollectionCreator;
        }

        public static bool IsSupportedCollectionType(Type type)
        {
            if (!type.IsGenericType)
                return false;
            Type typeDefinition = type.GetGenericTypeDefinition();
            return GetSupportedCollection(typeDefinition) == null;
        }
    }
}