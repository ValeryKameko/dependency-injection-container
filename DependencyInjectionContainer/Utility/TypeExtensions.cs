using System;
using System.Collections.Generic;
using System.Linq;

namespace DependencyInjectionContainer.Utility
{
    public static class TypeExtensions
    {
        public static bool IsAssignableFromGeneric(this Type toType, Type fromType)
        {
            IEnumerable<Type> baseTypes = GetBaseTypes(fromType.GetTypeDefinition());
            return baseTypes
                .Select(type => type.GetTypeDefinition())
                .Contains(toType.GetTypeDefinition());
        }

        private static Type GetTypeDefinition(this Type type) =>
            type.IsGenericType ? type.GetGenericTypeDefinition() : type;

        private static IEnumerable<Type> GetBaseTypes(Type type)
        {
            for (Type baseType = type; baseType != null; baseType = baseType.BaseType)
                yield return baseType;

            IEnumerable<Type> interfaceTypes =
                from Type interfaceType in type.GetInterfaces()
                select interfaceType;

            foreach (Type interfaceType in interfaceTypes)
                yield return interfaceType;
        }
    }
}