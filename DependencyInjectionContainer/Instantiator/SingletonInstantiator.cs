using System;
using System.Collections.Concurrent;
using DependencyInjectionContainer.Container;

namespace DependencyInjectionContainer.Instantiator
{
    internal class SingletonInstantiator : IInstantiator
    {
        private readonly ConcurrentDictionary<Type, Lazy<object>> _instanceByType =
            new ConcurrentDictionary<Type, Lazy<object>>();

        private readonly IInstantiator _instantiator;

        public SingletonInstantiator(IInstantiator instantiator) =>
            _instantiator = instantiator;

        public object Instantiate(Type type, AbstractDependencyContainer container) =>
            _instanceByType.GetOrAdd(type,
                key => new Lazy<object>(() => _instantiator.Instantiate(type, container)))
                .Value;
    }
}