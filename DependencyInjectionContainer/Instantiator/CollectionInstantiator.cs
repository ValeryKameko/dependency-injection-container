using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DependencyInjectionContainer.Container;
using DependencyInjectionContainer.Utility;
using static DependencyInjectionContainer.Utility.CollectionsHelper;

namespace DependencyInjectionContainer.Instantiator
{
    internal class CollectionInstantiator : IInstantiator
    {
        private readonly IEnumerable<Dependency> _dependencies;

        public CollectionInstantiator(IEnumerable<Dependency> dependencies) => 
            _dependencies = dependencies;

        public object Instantiate(Type type, AbstractDependencyContainer container)
        {
            IEnumerable<object> instances = _dependencies.Select(dependency =>
                dependency.Instantiator.Instantiate(dependency.Type, container));

            CollectionCreator containerCreator = GetCollectionCreator(type.GetGenericTypeDefinition());

            return containerCreator.Invoke(type.GetGenericArguments()[0], instances);
        }
    }
}