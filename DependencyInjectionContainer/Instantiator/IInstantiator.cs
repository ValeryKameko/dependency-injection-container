using System;
using DependencyInjectionContainer.Container;

namespace DependencyInjectionContainer.Instantiator
{
    internal interface IInstantiator
    {
        object Instantiate(Type type, AbstractDependencyContainer container);
    }
}