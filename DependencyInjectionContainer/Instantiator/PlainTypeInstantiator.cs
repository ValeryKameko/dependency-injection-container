using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Metadata;
using DependencyInjectionContainer.Container;
using DependencyInjectionContainer.Exception;

namespace DependencyInjectionContainer.Instantiator
{
    internal class PlainTypeInstantiator : IInstantiator
    {
        public object Instantiate(Type type, AbstractDependencyContainer container)
        {
            IEnumerable<ConstructorInfo> constructors = ChooseConstructors(type);
            if (!constructors.Any()) throw new DependencyException("Type has no injectable constructor");
            foreach (ConstructorInfo constructor in constructors)
            {
                try
                {
                    IEnumerable<ParameterInfo> parameters = constructor.GetParameters();
                    IEnumerable<object> arguments = InjectParameters(container, parameters);
                    return constructor.Invoke(arguments.ToArray());
                }
                catch (InjectionException)
                {
                }
            }

            throw new InjectionException($"Constructor injection failed for {type}");
        }

        private IEnumerable<object> InjectParameters(AbstractDependencyContainer _container,
            IEnumerable<ParameterInfo> parameters)
        {
            return parameters.Select(_container.Resolve);
        }

        private IEnumerable<ConstructorInfo> ChooseConstructors(Type type)
        {
            return type.GetConstructors()
                .Where(IsConstructorInjectable);
        }

        private bool IsConstructorInjectable(ConstructorInfo constructor)
        {
            return constructor.GetParameters()
                .All(IsParameterConstructable);
        }

        private static bool IsParameterConstructable(ParameterInfo parameter)
        {
            Type parameterType = parameter.GetType();
            return parameterType.IsClass;
        }
    }
}