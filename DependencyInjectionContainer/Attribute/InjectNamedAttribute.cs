using System;

namespace DependencyInjectionContainer.Attribute
{
    [AttributeUsage(AttributeTargets.Parameter)]
    public sealed class InjectNamedAttribute : System.Attribute
    {
        public object Name { get; }

        public InjectNamedAttribute(object name) =>
            Name = name;
    }
}