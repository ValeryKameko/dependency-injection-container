using System;
using System.Reflection;
using DependencyInjectionContainer.Exception;

namespace DependencyInjectionContainer.Container
{
    public static class DependencyContainerExtensions
    {
        public static T Resolve<T>(this AbstractDependencyContainer container) {
            try
            {
                return (T) container.Resolve(typeof(T));
            }
            catch (InvalidCastException e)
            {
                throw new InjectionException("Cannot instantiate dependency", e);
            }
        }
        
        public static T ResolveNamed<T>(this AbstractDependencyContainer container, object name) {
            try
            {
                return (T) container.ResolveNamed(typeof(T), name);
            }
            catch (InvalidCastException e)
            {
                throw new InjectionException("Cannot instantiate dependency", e);
            }
        }
    }
}