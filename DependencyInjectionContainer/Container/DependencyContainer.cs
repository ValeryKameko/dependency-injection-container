using System;
using System.ComponentModel.Design.Serialization;
using System.Reflection;
using DependencyInjectionContainer.DependencyKey;
using DependencyInjectionContainer.DependencyResolver;
using DependencyInjectionContainer.Exception;

namespace DependencyInjectionContainer.Container
{
    public class DependencyContainer : AbstractDependencyContainer
    {
        private readonly IDependencyResolver _resolver;

        public DependencyContainer(AbstractContainerConfiguration configuration)
        {
            IDependencyRegistry registry = configuration.DependencyRegistry;
            _resolver = new CompositeDependencyResolver(registry);
        }
        
        public override object Resolve(Type type)
        {
            CheckType(type);
            IDependencyKey key = DependencyKeyFactory.FromType(type);
            return ResolveDependency(type, _resolver, key);
        }

        internal override object Resolve(ParameterInfo parameter)
        {
            CheckType(parameter.ParameterType);
            IDependencyKey key = DependencyKeyFactory.FromParameter(parameter);
            return ResolveDependency(parameter.ParameterType, _resolver, key);
        }
        
        public override object ResolveNamed(Type type, object name)
        {
            CheckType(type);
            IDependencyKey key = DependencyKeyFactory.FromNamedType(type, name);
            return ResolveDependency(type, _resolver, key);
        }

        private object ResolveDependency(Type type, IDependencyResolver resolver, IDependencyKey key)
        {
            Dependency dependency = resolver.Resolve(key);
            if (dependency == null)
                throw new InjectionException($"Dependency for type {type} is not registered");
            return dependency.Instantiator.Instantiate(dependency.Type,this);
        }
        
        private static void CheckType(Type type)
        {
            if (type.ContainsGenericParameters)
                throw new InjectionException("Injecting type is open generic");
            if (!type.IsInterface && !type.IsClass)
                throw new InjectionException("Injecting type is not class or interface");
        }
    }
}