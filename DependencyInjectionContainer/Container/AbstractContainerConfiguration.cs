using System;

namespace DependencyInjectionContainer.Container
{
    public abstract class AbstractContainerConfiguration
    {
        internal IDependencyRegistry DependencyRegistry { get; }
        internal IDependencyCreator DependencyCreator { get; }

        internal AbstractContainerConfiguration(
            IDependencyRegistry dependencyRegistry,
            IDependencyCreator dependencyCreator)
        {
            DependencyRegistry = dependencyRegistry;
            DependencyCreator = dependencyCreator;
        }

        public abstract void Register(Type keyType, Type dependencyType, LifeType lifeType = LifeType.InstancePerDependency);
        
        public abstract void RegisterNamed(Type keyType, Type dependencyType,
            object name, LifeType lifeType = LifeType.InstancePerDependency);
    }
}