namespace DependencyInjectionContainer.Container
{
    public static class ContainerConfigurationExtensions
    {
        public static void Register<TKey, TDependency>(
            this AbstractContainerConfiguration configuration,
            LifeType lifeType = LifeType.InstancePerDependency)
            where TKey : class
            where TDependency : class, TKey =>
            configuration.Register(typeof(TKey), typeof(TDependency), lifeType);

        public static void RegisterNamed<TKey, TDependency>(
            this AbstractContainerConfiguration configuration,
            object name,
            LifeType lifeType = LifeType.InstancePerDependency)
            where TKey : class
            where TDependency : class, TKey =>
            configuration.RegisterNamed(typeof(TKey), typeof(TDependency), name, lifeType);
    }
}