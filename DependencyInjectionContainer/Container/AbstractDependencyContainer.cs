using System;
using System.Reflection;
using System.Reflection.Metadata;

namespace DependencyInjectionContainer.Container
{
    public abstract class AbstractDependencyContainer
    {
        public abstract object Resolve(Type type);
        internal abstract object Resolve(ParameterInfo parameter);

        public abstract object ResolveNamed(Type type, object name);
    }
}