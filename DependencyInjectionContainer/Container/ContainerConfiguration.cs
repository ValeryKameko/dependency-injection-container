using System;
using DependencyInjectionContainer.DependencyKey;
using DependencyInjectionContainer.Exception;
using DependencyInjectionContainer.Utility;

namespace DependencyInjectionContainer.Container
{
    public class ContainerConfiguration : AbstractContainerConfiguration
    {
        internal ContainerConfiguration(IDependencyRegistry dependencyRegistry, IDependencyCreator dependencyCreator)
            : base(dependencyRegistry, dependencyCreator)
        {
        }

        public ContainerConfiguration()
            : this(new DependencyRegistry(), new DependencyCreator())
        {
        }

        public override void Register(Type keyType, Type dependencyType,
            LifeType lifeType = LifeType.InstancePerDependency)
        {
            CheckTypes(keyType, dependencyType);
            IDependencyKey dependencyKey = DependencyKeyFactory.FromType(keyType);
            Dependency dependency = DependencyCreator.CreateFromType(dependencyType, lifeType);
            DependencyRegistry.Register(dependencyKey, dependency);

        }

        public override void RegisterNamed(Type keyType, Type dependencyType, object name,
            LifeType lifeType = LifeType.InstancePerDependency)
        {
            if (name == null) throw new ArgumentNullException(nameof(name));
            CheckTypes(keyType, dependencyType);
            IDependencyKey dependencyKey = DependencyKeyFactory.FromNamedType(keyType, name);
            Dependency dependency = DependencyCreator.CreateFromType(dependencyType, lifeType);
            DependencyRegistry.Register(dependencyKey, dependency);
        }

        private void CheckTypes(Type keyType, Type dependencyType)
        {
            if (keyType == null) throw new ArgumentNullException(nameof(keyType));
            if (dependencyType == null) throw new ArgumentNullException(nameof(dependencyType));
            if (!dependencyType.IsClass)
                throw new DependencyException("Dependency must be registered with reference type");
            if (!keyType.IsAssignableFromGeneric(dependencyType))
                throw new DependencyException("Dependency must be subtype of keyType");
            if (dependencyType.IsAbstract)
                throw new DependencyException($"{nameof(dependencyType)} type must be non abstract");
        }
    }
}