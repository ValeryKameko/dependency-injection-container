using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using DependencyInjectionContainer.DependencyKey;
using DependencyInjectionContainer.Exception;
using DependencyInjectionContainer.Instantiator;
using DependencyInjectionContainer.Utility;
using static DependencyInjectionContainer.Utility.CollectionsHelper;

namespace DependencyInjectionContainer.DependencyResolver
{
    internal class CollectionTypeDependencyResolver : IDependencyResolver
    {
        private readonly IDependencyRegistry _registry;
        private readonly IDependencyResolver _resolver;

        public CollectionTypeDependencyResolver(
            IDependencyRegistry registry,
            IDependencyResolver resolver)
        {
            _registry = registry;
            _resolver = resolver;
        }

        public Dependency Resolve(IDependencyKey key)
        {
            if (!key.KeyType.IsGenericType)
                return null;
            Type keyTypeDefinition = key.KeyType.GetGenericTypeDefinition();
            Type resolvedTypeDefinition = GetSupportedCollection(keyTypeDefinition);
            if (resolvedTypeDefinition == null)
                return null;

            Type[] keyTypeParameters = key.KeyType.GetGenericArguments();
            if (keyTypeParameters.Length != 1 || keyTypeParameters.Any(IsSupportedCollectionType))
                return null;

            Type resolvedCollection = resolvedTypeDefinition.MakeGenericType(keyTypeParameters);

            key.KeyType = keyTypeParameters[0];
            IEnumerable<Dependency> dependencies =
                _resolver.ResolveAll(key);
            if (!dependencies.Any())
                throw new InjectionException("No registered dependencies found");
            return new Dependency(resolvedCollection, new CollectionInstantiator(dependencies));
        }

        public IEnumerable<Dependency> ResolveAll(IDependencyKey key)
        {
            return Enumerable.Empty<Dependency>();
        }
    }
}