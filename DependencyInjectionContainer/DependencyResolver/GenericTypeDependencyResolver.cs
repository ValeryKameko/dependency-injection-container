using System;
using System.Collections.Generic;
using System.Linq;
using DependencyInjectionContainer.DependencyKey;
using DependencyInjectionContainer.Instantiator;

namespace DependencyInjectionContainer.DependencyResolver
{
    internal class GenericTypeDependencyResolver : IDependencyResolver
    {
        private readonly IDependencyResolver _plainResolver;

        public GenericTypeDependencyResolver(IDependencyRegistry registry) =>
            _plainResolver = new PlainTypeDependencyResolver(registry);

        public Dependency Resolve(IDependencyKey key)
        {
            Type keyType = key.KeyType;
            if (!keyType.IsGenericType)
                return null;

            Type[] arguments = keyType.GetGenericArguments();
            key.KeyType = keyType.GetGenericTypeDefinition();

            Dependency dependency = _plainResolver.Resolve(key);
            if (dependency == null)
                return null;

            Type resolvedType = dependency.Type.MakeGenericType(arguments);
            return new Dependency(resolvedType, dependency.Instantiator);
        }

        public IEnumerable<Dependency> ResolveAll(IDependencyKey key)
        {
            Type keyType = key.KeyType;
            if (!keyType.IsGenericType)
                return Enumerable.Empty<Dependency>();

            Type[] arguments = keyType.GetGenericArguments();
            key.KeyType = keyType.GetGenericTypeDefinition();

            IEnumerable<Dependency> dependencies = _plainResolver.ResolveAll(key);
            return dependencies.Select(dependency =>
            {
                Type resolvedType = dependency.Type.MakeGenericType(arguments);
                return new Dependency(resolvedType, dependency.Instantiator);
            });
        }
    }
}