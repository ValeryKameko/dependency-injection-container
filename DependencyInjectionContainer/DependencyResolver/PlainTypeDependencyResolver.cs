using System.Collections.Generic;
using System.Linq;
using DependencyInjectionContainer.DependencyKey;

namespace DependencyInjectionContainer.DependencyResolver
{
    internal class PlainTypeDependencyResolver : IDependencyResolver
    {
        private readonly IDependencyRegistry _dependencyRegistry;
        
        public PlainTypeDependencyResolver(IDependencyRegistry dependencyRegistry) => 
            _dependencyRegistry = dependencyRegistry;

        public Dependency Resolve(IDependencyKey key)
        {
            bool isFound = _dependencyRegistry.TryGet(key, out Dependency dependency);
            return isFound ? dependency : null;
        }
        
        public IEnumerable<Dependency> ResolveAll(IDependencyKey key)
        {
            bool isFound = _dependencyRegistry.TryGetAll(key, out IEnumerable<Dependency> dependencies);
            return isFound ? dependencies : Enumerable.Empty<Dependency>();
        }
    }
}