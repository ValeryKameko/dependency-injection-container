using System.Collections.Generic;
using System.Linq;
using DependencyInjectionContainer.DependencyKey;

namespace DependencyInjectionContainer.DependencyResolver
{
    internal class CompositeDependencyResolver : IDependencyResolver
    {
        private readonly IEnumerable<IDependencyResolver> _resolveSequence;

        public CompositeDependencyResolver(IDependencyRegistry registry)
        {
            var plainTypeDependencyResolver = new PlainTypeDependencyResolver(registry);
            var genericTypeDependencyResolver = new GenericTypeDependencyResolver(registry);
            var collectionDependencyResolver = new CollectionTypeDependencyResolver(registry, this);
            _resolveSequence = new List<IDependencyResolver>
            {
                collectionDependencyResolver,
                plainTypeDependencyResolver,
                genericTypeDependencyResolver
            };
        }

        public Dependency Resolve(IDependencyKey key)
        {
            return _resolveSequence
                .Select(resolver => resolver.Resolve(key))
                .FirstOrDefault(dependency => dependency != null);
        }
        
        public IEnumerable<Dependency> ResolveAll(IDependencyKey key)
        {
            return _resolveSequence.SelectMany(resolver => resolver.ResolveAll(key));
        }
    }
}