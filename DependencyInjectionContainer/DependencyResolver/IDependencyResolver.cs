using System.Collections.Generic;
using DependencyInjectionContainer.DependencyKey;

namespace DependencyInjectionContainer.DependencyResolver
{
    internal interface IDependencyResolver
    {
        Dependency Resolve(IDependencyKey key);
        IEnumerable<Dependency> ResolveAll(IDependencyKey key);
    }
}