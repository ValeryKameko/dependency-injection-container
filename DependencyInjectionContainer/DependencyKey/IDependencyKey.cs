using System;

namespace DependencyInjectionContainer.DependencyKey
{
    internal interface IDependencyKey
    {
        Type KeyType { get; set; }
    }
}