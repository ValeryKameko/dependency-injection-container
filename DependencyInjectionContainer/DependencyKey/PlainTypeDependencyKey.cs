using System;

namespace DependencyInjectionContainer.DependencyKey
{
    internal class PlainTypeDependencyKey : IDependencyKey
    {
        public PlainTypeDependencyKey(Type keyType)
        {
            KeyType = keyType;
        }

        public Type KeyType { get; set; }

        protected bool Equals(PlainTypeDependencyKey other)
        {
            return Equals(KeyType, other.KeyType);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((PlainTypeDependencyKey) obj);
        }

        public override int GetHashCode()
        {
            return KeyType != null ? KeyType.GetHashCode() : 0;
        }
    }
}