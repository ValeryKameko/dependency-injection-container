using System;

namespace DependencyInjectionContainer.DependencyKey
{
    internal class NamedTypeDependencyKey : PlainTypeDependencyKey
    {
        private readonly object _name;

        public NamedTypeDependencyKey(Type keyType, object name) : base(keyType) => 
            _name = name;

        protected bool Equals(NamedTypeDependencyKey other)
        {
            return base.Equals(other) && Equals(_name, other._name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != GetType()) return false;
            return Equals((NamedTypeDependencyKey) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (base.GetHashCode() * 397) ^ (_name != null ? _name.GetHashCode() : 0);
            }
        }
    }
}