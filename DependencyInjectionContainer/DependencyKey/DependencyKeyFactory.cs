using System;
using System.Reflection;
using DependencyInjectionContainer.Attribute;

namespace DependencyInjectionContainer.DependencyKey
{
    internal static class DependencyKeyFactory
    {
        public static IDependencyKey FromParameter(ParameterInfo parameter)
        {
            var namedAttribute = parameter.GetCustomAttribute<InjectNamedAttribute>();
            if (namedAttribute != null)
                return new NamedTypeDependencyKey(parameter.ParameterType, namedAttribute.Name);
            return new PlainTypeDependencyKey(parameter.ParameterType);
        }
        
        public static IDependencyKey FromType(Type type) => 
            new PlainTypeDependencyKey(type);

        public static IDependencyKey FromNamedType(Type type, object name) => 
            new NamedTypeDependencyKey(type, name);
    }
}