using System.Collections.Generic;
using DependencyInjectionContainer.DependencyKey;

namespace DependencyInjectionContainer
{
    internal interface IDependencyRegistry
    {
        void Register(IDependencyKey key, Dependency dependency);
        bool TryGet(IDependencyKey key, out Dependency dependency);
        bool TryGetAll(IDependencyKey key, out IEnumerable<Dependency> dependencies);
    }
}