using System;

namespace DependencyInjectionContainer
{
    internal interface IDependencyCreator
    {
        Dependency CreateFromType(Type type, LifeType lifeType);
    }
}