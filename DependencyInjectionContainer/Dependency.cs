using System;
using DependencyInjectionContainer.Instantiator;

namespace DependencyInjectionContainer
{
    internal class Dependency
    {
        public Dependency(Type type, IInstantiator instantiator)
        {
            Type = type;
            Instantiator = instantiator;
        }

        public Type Type { get; }
        public IInstantiator Instantiator { get; }
    }
}