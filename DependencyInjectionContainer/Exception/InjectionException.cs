using System.Runtime.Serialization;

namespace DependencyInjectionContainer.Exception
{
    public class InjectionException : System.Exception
    {
        public InjectionException()
        {
        }

        protected InjectionException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        public InjectionException(string message) : base(message)
        {
        }

        public InjectionException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}