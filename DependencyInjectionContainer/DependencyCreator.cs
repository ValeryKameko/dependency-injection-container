using System;
using DependencyInjectionContainer.Instantiator;

namespace DependencyInjectionContainer
{
    internal class DependencyCreator : IDependencyCreator
    {
        private static readonly IInstantiator PlainTypeInstantiator =
            new PlainTypeInstantiator();

        public Dependency CreateFromType(Type type, LifeType lifeType)
        {
            return new Dependency(type, ChooseInstantiator(lifeType));
        }

        private IInstantiator ChooseInstantiator(LifeType lifeType)
        {
            return lifeType switch
            {
                LifeType.Singleton => new SingletonInstantiator(PlainTypeInstantiator),
                LifeType.InstancePerDependency => PlainTypeInstantiator,
                _ => throw new ArgumentOutOfRangeException(nameof(lifeType), lifeType, null)
            };
        }
    }
}