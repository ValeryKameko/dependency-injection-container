using DependencyInjectionContainer.DependencyKey;
using DependencyInjectionContainer.DependencyResolver;
using DependencyInjectionContainer.Instantiator;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using static DependencyInjectionContainer.Test.TypeCase.SingleTypeCase;

namespace DependencyInjectionContainer.Test.Resolver
{
    [TestFixture]
    public class PlainTypeDependencyResolverTests
    {
        private Mock<IDependencyRegistry> _dependencyRegistryMock;
        private Mock<IInstantiator> _instantiatorMock;
        private PlainTypeDependencyResolver _dependencyResolver;

        [SetUp]
        public void SetUp()
        {
            var dependencyRegistry = Mock.Of<IDependencyRegistry>();
            _dependencyResolver = new PlainTypeDependencyResolver(dependencyRegistry);

            _dependencyRegistryMock = Mock.Get(dependencyRegistry);

            _instantiatorMock = new Mock<IInstantiator>();
        }

        [Test]
        public void Resolve_Should_ProvideDependencyForPlainTypeQuery()
        {
            var key = new PlainTypeDependencyKey(SingleType);
            var registeredDependency = new Dependency(SingleType, _instantiatorMock.Object);
            _dependencyRegistryMock.Setup(registry => registry.TryGet(key, out registeredDependency)).Returns(true);

            Dependency resolvedDependency = _dependencyResolver.Resolve(key);
            
            resolvedDependency.Should().BeEquivalentTo(registeredDependency);
            _dependencyRegistryMock.Verify();
            _instantiatorMock.VerifyNoOtherCalls();
        }
    }
}