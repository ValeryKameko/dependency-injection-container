using DependencyInjectionContainer.Container;
using static DependencyInjectionContainer.Test.TypeCase.GenericTypeCase;
using DependencyInjectionContainer.DependencyKey;
using DependencyInjectionContainer.DependencyResolver;
using DependencyInjectionContainer.Instantiator;
using FluentAssertions;
using Moq;
using NUnit.Framework;

namespace DependencyInjectionContainer.Test.Resolver
{
    [TestFixture]
    public class GenericTypeDependencyResolverTests
    {
        private Mock<IDependencyRegistry> _registryMock;
        private Mock<IInstantiator> _instantiatorMock;
        private GenericTypeDependencyResolver _resolver;

        [SetUp]
        public void SetUp()
        {
            _registryMock = new Mock<IDependencyRegistry>();
            _instantiatorMock = new Mock<IInstantiator>();
            
            _resolver = new GenericTypeDependencyResolver(_registryMock.Object);
        }

        [Test]
        public void Resolve_Should_ProvideDependencyForGenericTypeQuery()
        {
            var genericKey = new PlainTypeDependencyKey(GenericType);
            var registeredDependency = new Dependency(GenericType, _instantiatorMock.Object);
            _registryMock.Setup(registry => registry.TryGet(genericKey, out registeredDependency))
                .Returns(true);
            var key = new PlainTypeDependencyKey(ConstructedGenericType);
            
            Dependency dependency = _resolver.Resolve(key);

            dependency.Type.Should().Be(ConstructedGenericType)
                .And.NotBeNull();
        }
        
        [Test]
        public void Resolve_Should_ProvideDependencyForGenericInterfaceTypeQuery()
        {
            var genericKey = new PlainTypeDependencyKey(GenericInterfaceType);
            var registeredDependency = new Dependency(GenericType, _instantiatorMock.Object);
            _registryMock.Setup(registry => registry.TryGet(genericKey, out registeredDependency))
                .Returns(true);
            var key = new PlainTypeDependencyKey(ConstructedGenericInterfaceType);
            
            Dependency dependency = _resolver.Resolve(key);

            dependency.Type.Should().Be(ConstructedGenericType)
                .And.NotBeNull();
        }
    }
}