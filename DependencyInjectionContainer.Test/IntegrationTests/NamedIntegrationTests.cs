using System;
using System.Collections.Generic;
using DependencyInjectionContainer.Container;
using DependencyInjectionContainer.Exception;
using FluentAssertions;
using NUnit.Framework;
using static DependencyInjectionContainer.Test.TypeCase.ConstructorInjectionTypeCase;
using static DependencyInjectionContainer.Test.TypeCase.SingleTypeCase;
using static DependencyInjectionContainer.Test.TypeCase.MultipleImplementationsTypeCase;

namespace DependencyInjectionContainer.Test.IntegrationTests
{
    [TestFixture]
    public class NamedIntegrationTests
    {
        [Test]
        public void ResolveNamed_Should_ProvideRegisteredNamedDependency()
        {
            var config = new ContainerConfiguration();
            config.RegisterNamed<SingleClass, SingleClass>("key");
            var container = new DependencyContainer(config);

            var instance = container.ResolveNamed<SingleClass>("key");

            instance.Should().NotBeNull();
        }

        [Test]
        public void ResolveNamed_Should_ProvideAllRegisteredNamedDependency()
        {
            var config = new ContainerConfiguration();
            config.RegisterNamed<IInterface, FirstImplementation>("impl");
            config.RegisterNamed<IInterface, SecondImplementation>("impl");
            var container = new DependencyContainer(config);

            var instances = container.ResolveNamed<IEnumerable<IInterface>>("impl");

            instances.Should().HaveCount(2);
        }

        [Test]
        public void ResolveNamed_Should_ProvideExactDependency_WhereDependenciesRegisteredWithDifferentKeys()
        {
            var config = new ContainerConfiguration();
            config.RegisterNamed<IInterface, FirstImplementation>("impl1");
            config.RegisterNamed<IInterface, SecondImplementation>("impl2");
            var container = new DependencyContainer(config);

            var firstInstance = container.ResolveNamed<IInterface>("impl1");
            var secondInstance = container.ResolveNamed<IInterface>("impl2");

            firstInstance.Should().BeOfType<FirstImplementation>();
            secondInstance.Should().BeOfType<SecondImplementation>();
        }

        [Test]
        public void ResolveNamed_ShouldFail_WhenNoKeyedDependenciesMatch()
        {
            var config = new ContainerConfiguration();
            config.RegisterNamed<SingleClass, SingleClass>("right");
            var container = new DependencyContainer(config);

            Action act = () => container.ResolveNamed<IInterface>("wrong");

            act.Should().Throw<InjectionException>();
        }

        [Test]
        public void ResolveNamed_ShouldInjectInstancesInParameters()
        {
            var config = new ContainerConfiguration();
            config.RegisterNamed<IInterface, SecondImplementation>("impl");
            config.Register(keyType: GenericNamedInjectableType,
                GenericNamedInjectableType);
            var container = new DependencyContainer(config);

            var instance = container.Resolve<GenericNamedInjectableClass<IInterface>>();

            instance.Should().NotBeNull();
            instance.InjectingInstance
                .Should().BeOfType<SecondImplementation>();
        }
    }
}