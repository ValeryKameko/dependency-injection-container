using System;
using System.Collections.Generic;
using System.Linq;
using DependencyInjectionContainer.Container;
using DependencyInjectionContainer.Exception;
using FluentAssertions;
using NUnit.Framework;
using static DependencyInjectionContainer.Test.TypeCase.MultipleImplementationsTypeCase;
using static DependencyInjectionContainer.Test.TypeCase.SingleTypeCase;

namespace DependencyInjectionContainer.Test.IntegrationTests
{
    [TestFixture]
    public class PlainTypeIntegrationTests
    {
        [Test]
        public void Resolve_Should_FailForNonRegisteredDependency()
        {
            var config = new ContainerConfiguration();
            var container = new DependencyContainer(config);

            Action act = () => container.Resolve<SingleClass>();

            act.Should().Throw<InjectionException>();
        }

        [Test]
        public void Resolve_Should_ProvideRegisteredDependency()
        {
            var config = new ContainerConfiguration();
            config.Register<SingleClass, SingleClass>();
            var container = new DependencyContainer(config);

            var instance = container.Resolve<SingleClass>();

            instance.Should().NotBeNull();
        }

        [Test]
        public void Resolve_Should_ProvideRegisteredDependencyForInterface()
        {
            var config = new ContainerConfiguration();
            config.Register<IInterface, FirstImplementation>();
            var container = new DependencyContainer(config);

            var instance = container.Resolve<IInterface>();

            instance.Should().BeOfType<FirstImplementation>()
                .And.NotBeNull();
        }

        [Test]
        public void Resolve_Should_ProvideRegisteredDependencyAsSingleton()
        {
            var config = new ContainerConfiguration();
            config.Register<IInterface, FirstImplementation>(LifeType.Singleton);
            var container = new DependencyContainer(config);

            var firstInstance = container.Resolve<IInterface>();
            var secondInstance = container.Resolve<IInterface>();

            firstInstance.Should().BeOfType<FirstImplementation>()
                .And.NotBeNull();
            secondInstance.Should().Be(firstInstance);
        }

        [Test]
        public void Resolve_Should_ProvideRegisteredDependencyAsPerDependency()
        {
            var config = new ContainerConfiguration();
            config.Register<IInterface, FirstImplementation>(LifeType.InstancePerDependency);
            var container = new DependencyContainer(config);

            var firstInstance = container.Resolve<IInterface>();
            var secondInstance = container.Resolve<IInterface>();

            firstInstance.Should().BeOfType<FirstImplementation>()
                .And.NotBeNull();
            secondInstance.Should().NotBe(firstInstance);
        }

        [Test]
        public void Resolve_Should_ProvideAllRegisteredDependencies()
        {
            var config = new ContainerConfiguration();
            config.Register<IInterface, FirstImplementation>();
            config.Register<IInterface, SecondImplementation>();
            var container = new DependencyContainer(config);

            var instances = container.Resolve<IEnumerable<IInterface>>();

            instances.Should().SatisfyRespectively(
                firstInstance => firstInstance.Should()
                    .BeOfType<FirstImplementation>()
                    .And.NotBeNull(),
                secondInstance => secondInstance.Should()
                    .BeOfType<SecondImplementation>()
                    .And.NotBeNull());
        }
    }
}