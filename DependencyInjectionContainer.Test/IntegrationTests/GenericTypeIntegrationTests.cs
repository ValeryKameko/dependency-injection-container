using System;
using DependencyInjectionContainer.Container;
using FluentAssertions;
using NUnit.Framework;
using static DependencyInjectionContainer.Test.TypeCase.SingleTypeCase;
using static DependencyInjectionContainer.Test.TypeCase.GenericTypeCase;
using static DependencyInjectionContainer.Test.TypeCase.MultipleImplementationsTypeCase;

namespace DependencyInjectionContainer.Test.IntegrationTests
{
    [TestFixture]
    public class GenericTypeIntegrationTests
    {
        [Test]
        public void Resolve_Should_ProvideRegisteredGenericDependency()
        {
            var config = new ContainerConfiguration();
            config.Register(GenericType, GenericType);
            var container = new DependencyContainer(config);

            var instance = container.Resolve<GenericClass<ConcreteClass>>();

            instance.Should().NotBeNull();
        }
        
        [Test]
        public void Resolve_Should_ProvideRegisteredGenericDependencyForInterface()
        {
            var config = new ContainerConfiguration();
            config.Register(GenericInterfaceType, GenericType);
            var container = new DependencyContainer(config);

            var instance = container.Resolve<IGenericInterface<ConcreteClass>>();

            instance.Should().BeOfType<GenericClass<ConcreteClass>>()
                .And.NotBeNull();
        }
    }
}