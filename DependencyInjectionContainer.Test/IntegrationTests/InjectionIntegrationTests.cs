using DependencyInjectionContainer.Container;
using FluentAssertions;
using NUnit.Framework;
using static DependencyInjectionContainer.Test.TypeCase.ConstructorInjectionTypeCase;
using static DependencyInjectionContainer.Test.TypeCase.GenericTypeCase;
using static DependencyInjectionContainer.Test.TypeCase.CollectionTypeCase;
using static DependencyInjectionContainer.Test.TypeCase.MultipleImplementationsTypeCase;

namespace DependencyInjectionContainer.Test.IntegrationTests
{
    [TestFixture]
    public class InjectionIntegrationTests
    {
        [Test]
        public void Resolve_Should_ProvideInstanceWithInjectedParameters()
        {
            var config = new ContainerConfiguration();
            config.Register<InjectingClass, InjectingClass>();
            config.Register<InjectableClass, InjectableClass>();
         
            var container = new DependencyContainer(config);

            var instance = container.Resolve<InjectableClass>();

            instance.Should().NotBeNull();
            instance.InjectingInstance.Should().NotBeNull();
        }
        
        [Test]
        public void Resolve_Should_InjectGenericParameterInConstructor()
        {
            var config = new ContainerConfiguration();
            config.Register(ConcreteType, ConcreteType);
            config.Register(GenericInjectableType, GenericInjectableType);
            var container = new DependencyContainer(config);

            var instance = container.Resolve<GenericInjectableClass<ConcreteClass>>();

            instance.Should().BeOfType<GenericInjectableClass<ConcreteClass>>()
                .And.NotBeNull();
        }
        
        [Test]
        public void Resolve_Should_InjectAllDependenciesInConstructor()
        {
            var config = new ContainerConfiguration();
            config.Register(CollectionInjectableType, CollectionInjectableType);
            config.Register<IInterface, FirstImplementation>();
            config.Register<IInterface, SecondImplementation>();
            var container = new DependencyContainer(config);

            var instance = container.Resolve<CollectionInjectableClass<IInterface>>();

            instance.Should().NotBeNull();
            instance.InjectingInstance.Should().HaveCount(2);
        }

    }
}
