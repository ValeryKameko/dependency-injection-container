using System.Collections.Generic;
using DependencyInjectionContainer.DependencyKey;
using DependencyInjectionContainer.Instantiator;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using static DependencyInjectionContainer.Test.TypeCase.SingleTypeCase;

namespace DependencyInjectionContainer.Test
{
    [TestFixture]
    public class DependencyRegistryTests
    {
        private DependencyRegistry _dependencyRegistry;

        private IInstantiator _instantiator;
        private Mock<IInstantiator> _instantiatorMock;

        [SetUp]
        public void SetUp()
        {
            _dependencyRegistry = new DependencyRegistry();

            _instantiator = Mock.Of<IInstantiator>();
            _instantiatorMock = Mock.Get(_instantiator);
        }

        [Test]
        public void Register_Should_RegisterDependency()
        {
            var key = new PlainTypeDependencyKey(SingleType);
            var dependency = new Dependency(SingleType, _instantiator);

            _dependencyRegistry.Register(key, dependency);
            bool isFound = _dependencyRegistry.TryGet(key, out Dependency foundDependency);

            isFound.Should().BeTrue();
            foundDependency.Should().Be(dependency);
            _instantiatorMock.VerifyNoOtherCalls();
        }

        [Test]
        public void 
            TryGetAll_Should_ProvideAllDependencies()
        {
            var key = new PlainTypeDependencyKey(SingleType);
            var firstDependency = new Dependency(SingleType, _instantiator);
            var secondDependency = new Dependency(SingleType, _instantiator);

            _dependencyRegistry.Register(key, firstDependency);
            _dependencyRegistry.Register(key, secondDependency);
            bool isFound = _dependencyRegistry.TryGetAll(key, out IEnumerable<Dependency> foundDependencies);

            isFound.Should().BeTrue();
            foundDependencies.Should().Equal(firstDependency, secondDependency);
            _instantiatorMock.VerifyNoOtherCalls();
        }
    }
}