using System;

namespace DependencyInjectionContainer.Test.TypeCase
{
    public static class GenericTypeCase
    {
        public class ConcreteClass
        {
        }

        public interface IGenericInterface<T>
        {
            
        }
        
        public class GenericClass<T> : IGenericInterface<T>
        {
        }


        public static readonly Type ConcreteType = typeof(ConcreteClass);
        public static readonly Type GenericInterfaceType = typeof(IGenericInterface<>);
        public static readonly Type ConstructedGenericInterfaceType = typeof(IGenericInterface<ConcreteClass>);
        public static readonly Type GenericType = typeof(GenericClass<>);
        public static readonly Type ConstructedGenericType = typeof(GenericClass<ConcreteClass>);
    }
}