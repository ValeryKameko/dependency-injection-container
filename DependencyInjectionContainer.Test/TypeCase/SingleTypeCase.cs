using System;

namespace DependencyInjectionContainer.Test.TypeCase
{
    public static class SingleTypeCase
    {
        public class SingleClass
        {
        }
        
        public static readonly Type SingleType = typeof(SingleClass);
    }
}