using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;

namespace DependencyInjectionContainer.Test.TypeCase
{
    public static class CollectionTypeCase
    {
        public class CollectionInjectableClass<T>
        {
            public IEnumerable<T> InjectingInstance { get; }

            public CollectionInjectableClass(IEnumerable<T> injectingInstances) => InjectingInstance = injectingInstances;
        }
        
        public static readonly Type CollectionInjectableType = typeof(CollectionInjectableClass<>);
    }
}