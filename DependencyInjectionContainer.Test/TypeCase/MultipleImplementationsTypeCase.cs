using System;

namespace DependencyInjectionContainer.Test.TypeCase
{
    public static class MultipleImplementationsTypeCase
    {
        public interface IInterface
        {        
        }

        public class FirstImplementation : IInterface
        {
        }
        
        public class SecondImplementation : IInterface
        {
        }

        public static readonly Type FirstType = typeof(FirstImplementation);
        public static readonly Type SecondType = typeof(SecondImplementation);
    }
}