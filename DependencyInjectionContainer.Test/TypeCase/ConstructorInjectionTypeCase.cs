using System;
using System.Reflection;
using DependencyInjectionContainer.Attribute;

namespace DependencyInjectionContainer.Test.TypeCase
{
    public static class ConstructorInjectionTypeCase
    {
        public class InjectingClass
        {
        }

        public class InjectableClass
        {
            public InjectingClass InjectingInstance { get; }

            public InjectableClass(InjectingClass injectingInstance) => InjectingInstance = injectingInstance;
        }

        public class GenericInjectableClass<T>
        {
            public T InjectingInstance { get; }

            public GenericInjectableClass(T injectingInstance) => 
                InjectingInstance = injectingInstance;
        }


        public class GenericNamedInjectableClass<T>
        {
            public T InjectingInstance { get; }

            public GenericNamedInjectableClass([InjectNamed("impl")] T injectingInstance) =>
                InjectingInstance = injectingInstance;
        }

        public static readonly Type InjectingType = typeof(InjectingClass);
        public static readonly Type InjectableType = typeof(InjectableClass);
        public static readonly Type GenericInjectableType = typeof(GenericInjectableClass<>);
        public static readonly Type GenericNamedInjectableType = typeof(GenericNamedInjectableClass<>);

        public static readonly ParameterInfo[] InjectingParameters =
            InjectableType.GetConstructors()[0].GetParameters();
    }
}