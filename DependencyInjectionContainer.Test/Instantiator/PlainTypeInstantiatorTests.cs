using DependencyInjectionContainer.Container;
using DependencyInjectionContainer.Instantiator;
using FluentAssertions;
using static DependencyInjectionContainer.Test.TypeCase.ConstructorInjectionTypeCase;
using Moq;
using NUnit.Framework;

namespace DependencyInjectionContainer.Test.Instantiator
{
    [TestFixture]
    public class PlainTypeInstantiatorTests
    {
        private Mock<AbstractDependencyContainer> _containerMock;
        private PlainTypeInstantiator _instantiator;

        [SetUp]
        public void SetUp()
        {
            _instantiator = new PlainTypeInstantiator();
            _containerMock = new Mock<AbstractDependencyContainer>();
        }

        [Test]
        public void Instantiate_Should_InstantiateDependencyWithInjectableConstructor()
        {
            var injectingInstance = new InjectingClass();
            _containerMock.Setup(container => container.Resolve(InjectingParameters[0]))
                .Returns(injectingInstance);

            var instance = _instantiator.Instantiate(InjectableType, _containerMock.Object) as InjectableClass;

            instance.Should().NotBeNull();
            instance.InjectingInstance.Should().Be(injectingInstance);
            
        }
    }
}