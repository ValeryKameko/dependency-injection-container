using DependencyInjectionContainer.Container;
using DependencyInjectionContainer.DependencyKey;
using DependencyInjectionContainer.Instantiator;
using FluentAssertions;
using static DependencyInjectionContainer.Test.TypeCase.SingleTypeCase;
using Moq;
using NUnit.Framework;

namespace DependencyInjectionContainer.Test
{
    [TestFixture]
    public class DependencyContainerTests
    {
        private DependencyContainer _container;

        private Mock<AbstractContainerConfiguration> _configurationMock;
        private Mock<IDependencyCreator> _dependencyCreatorMock;
        private Mock<IDependencyRegistry> _registryMock;

        [SetUp]
        public void SetUp()
        {
            _dependencyCreatorMock = new Mock<IDependencyCreator>();
            _registryMock = new Mock<IDependencyRegistry>();
            _configurationMock = new Mock<AbstractContainerConfiguration>(
                _registryMock.Object,
                _dependencyCreatorMock.Object);
            _container = new DependencyContainer(_configurationMock.Object);
        }

        [TearDown]
        public void TearDown()
        {
            _configurationMock.Verify();
            _dependencyCreatorMock.Verify();
            _registryMock.Verify();
        }

        [Test]
        public void Resolve_Should_MakeRightQuery()
        {
            var expectedInstance = new SingleClass();
            var instantiatorMock = new Mock<IInstantiator>();
            var dependency = new Dependency(SingleType, instantiatorMock.Object);
            instantiatorMock.Setup(instantiator => instantiator.Instantiate(SingleType, _container))
                .Returns(expectedInstance);
            _registryMock.Setup(registry =>
                    registry.TryGet(new PlainTypeDependencyKey(SingleType), out dependency))
                .Returns(true);

            var resolvedInstance = _container.Resolve<SingleClass>();

            resolvedInstance.Should().Be(expectedInstance);
            instantiatorMock.Verify();
        }
    }
}