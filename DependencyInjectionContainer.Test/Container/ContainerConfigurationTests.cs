using DependencyInjectionContainer.Container;
using DependencyInjectionContainer.DependencyKey;
using DependencyInjectionContainer.Instantiator;
using Moq;
using NUnit.Framework;
using static DependencyInjectionContainer.Test.TypeCase.SingleTypeCase;

namespace DependencyInjectionContainer.Test.Container
{
    [TestFixture]
    public class ContainerConfigurationTests
    {
        private ContainerConfiguration _containerConfiguration;
        
        private Mock<IDependencyRegistry> _dependencyRegistryMock;
        private Mock<IDependencyCreator> _dependencyCreatorMock;
        private IInstantiator _instantiator;
        private Mock<IInstantiator> _instantiatorMock;


        [SetUp]
        public void SetUp()
        {
            var dependencyRegistry = Mock.Of<IDependencyRegistry>();
            var dependencyCreator = Mock.Of<IDependencyCreator>();

            _containerConfiguration = new ContainerConfiguration(dependencyRegistry, dependencyCreator);

            _dependencyCreatorMock = Mock.Get(dependencyCreator);
            _dependencyRegistryMock = Mock.Get(dependencyRegistry);

            _instantiator = Mock.Of<IInstantiator>();
            _instantiatorMock = Mock.Get(_instantiator);
        }

        [Test]
        public void Register_Should_RegisterDependency()
        {
            var dependency = new Dependency(SingleType, _instantiator);
            _dependencyCreatorMock.Setup(creator => creator.CreateFromType(SingleType, LifeType.InstancePerDependency))
                .Returns(dependency);

            _containerConfiguration.Register(SingleType, SingleType);

            _dependencyCreatorMock.Verify(creator => creator.CreateFromType(SingleType, LifeType.InstancePerDependency));
            _dependencyRegistryMock
                .Verify(registry => registry.Register(It.IsAny<PlainTypeDependencyKey>(), dependency));
            _instantiatorMock.VerifyNoOtherCalls();
        }
    }
}