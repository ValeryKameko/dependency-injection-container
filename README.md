# Dependency Injection Container
[![pipeline status](https://gitlab.com/ValeryKameko/dependency-injection-container/badges/master/pipeline.svg)](https://gitlab.com/ValeryKameko/dependency-injection-container/commits/master)
[![coverage report](https://gitlab.com/ValeryKameko/dependency-injection-container/badges/master/coverage.svg)](https://gitlab.com/ValeryKameko/dependency-injection-container/commits/master)

Dependency Injection Container written on C#

